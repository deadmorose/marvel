package ru.codegang.marvel.application.dependencies.activities.modules

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.codegang.marvel.activities.main.MainActivity
import ru.codegang.marvel.activities.main.MainActivityContract
import ru.codegang.marvel.activities.main.MainActivityViewModel
import ru.codegang.marvel.application.dependencies.models.DaggerViewModelFactory

@Module
class MainActivityModule {

    @Provides
    fun provideViewModel(
        activity: MainActivity,
        viewModelFactory: DaggerViewModelFactory
    ): MainActivityContract.ViewModel =
        ViewModelProvider(activity, viewModelFactory).get(MainActivityViewModel::class.java)
}