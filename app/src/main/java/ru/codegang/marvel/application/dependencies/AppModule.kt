package ru.codegang.marvel.application.dependencies

import android.app.Application
import dagger.Module
import dagger.Provides
import dagger.android.support.AndroidSupportInjectionModule
import ru.codegang.marvel.application.dependencies.activities.ActivityModule
import ru.codegang.marvel.application.dependencies.api.CloudApiModule
import ru.codegang.marvel.application.dependencies.models.ViewModelModule
import javax.inject.Singleton

@Module(
    includes = [
        AndroidSupportInjectionModule::class,
        ActivityModule::class,
        ViewModelModule::class,
        CloudApiModule::class
    ]
)
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    fun providesApplication(): Application = application
}