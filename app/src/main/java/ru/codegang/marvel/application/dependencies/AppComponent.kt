package ru.codegang.marvel.application.dependencies

import ru.codegang.marvel.application.App
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent{
    fun inject(app: App)
}