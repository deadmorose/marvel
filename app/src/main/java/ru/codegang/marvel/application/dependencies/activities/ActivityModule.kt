package ru.codegang.marvel.application.dependencies.activities

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.codegang.marvel.activities.main.MainActivity
import ru.codegang.marvel.application.dependencies.activities.modules.MainActivityModule
import ru.codegang.marvel.application.dependencies.scopes.ActivityScope

@Module
abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun mainActivity(): MainActivity
}