package ru.codegang.marvel.application.dependencies.api

import com.jaredsburrows.retrofit2.adapter.synchronous.SynchronousCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import ru.codegang.marvel.BuildConfig
import ru.codegang.marvel.network.CloudApi
import ru.codegang.marvel.network.CloudInterceptor
import ru.codegang.marvel.settings.DefaultSettings
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class CloudApiModule {

    private fun defaultConverter(): Converter.Factory = JacksonConverterFactory.create()

    private fun defaultCallAdapter(): CallAdapter.Factory =
        SynchronousCallAdapterFactory.create()

    private fun buildRetrofit(baseUrl: String, client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(defaultConverter())
            .addCallAdapterFactory(defaultCallAdapter())
            .client(client)
            .baseUrl(baseUrl)
            .build()
    }

    private fun getLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = DefaultSettings.HTTP_LOG_LEVEL
        }
    }

    @Provides
    @Singleton
    fun providesCloudAPI(cloudInterceptor: CloudInterceptor): CloudApi {
        val okHttpClient = OkHttpClient.Builder()
            .connectionSpecs(listOf(ConnectionSpec.COMPATIBLE_TLS, ConnectionSpec.CLEARTEXT))
            .connectTimeout(DefaultSettings.NETWORK_CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(DefaultSettings.NETWORK_READ_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(getLoggingInterceptor())
            .addInterceptor(cloudInterceptor)
            .build()

        val retrofit = buildRetrofit(BuildConfig.HOST_URL, okHttpClient)

        return retrofit.create(CloudApi::class.java)
    }
}