package ru.codegang.marvel.activities.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.codegang.marvel.base.BaseViewModel
import ru.codegang.marvel.base.SingleMutableLiveData
import ru.codegang.marvel.network.CloudApi
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(
    private val cloudApi: CloudApi
) : BaseViewModel(), MainActivityContract.ViewModel {
    override val error: SingleMutableLiveData<String>
        get() = TODO("Not yet implemented")
    override val loading: LiveData<Boolean>
        get() = TODO("Not yet implemented")

    override fun test() {
        viewModelScope.launch(Dispatchers.IO) {
            val characters = cloudApi.getCharacters()
        }
    }
}