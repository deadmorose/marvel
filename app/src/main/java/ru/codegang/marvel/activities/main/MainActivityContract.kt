package ru.codegang.marvel.activities.main

interface MainActivityContract {
    interface ViewModel{
        fun test()
    }
}