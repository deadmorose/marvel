package ru.codegang.marvel.activities.main

import android.os.Bundle
import ru.codegang.marvel.R
import ru.codegang.marvel.base.BaseActivity
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: MainActivityContract.ViewModel

    override fun getFragmentPlaceHolder(): Int {
        TODO("Not yet implemented")
    }

    override fun getContentView(): Int  = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.test()

    }

}