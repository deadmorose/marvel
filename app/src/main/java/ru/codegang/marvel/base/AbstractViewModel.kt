package ru.codegang.marvel.base

interface AbstractViewModel : ModelErrorHolder, ModelLoadingHolder