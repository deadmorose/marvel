package ru.codegang.marvel.base

import androidx.lifecycle.LiveData


interface ModelLoadingHolder {

    val loading: LiveData<Boolean>
}