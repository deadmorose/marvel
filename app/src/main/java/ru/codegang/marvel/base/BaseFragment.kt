package ru.codegang.marvel.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import dagger.android.support.DaggerFragment
import ru.codegang.marvel.base.BaseActivity.Companion.EXTRA_DATA_VALUE
import kotlin.reflect.KClass

abstract class BaseFragment : DaggerFragment() {
    private val objectMapper: ObjectMapper = ObjectMapper().apply {
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        //disable throwing exception when deserialization meet unknown property
    }

    abstract fun getContentView(): Int

    override fun onDestroyView() {
        hideKeyboard()
        super.onDestroyView()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(getContentView(), container, false)

    fun getBaseActivity() = activity as BaseActivity

    open fun getNavController() = Navigation.findNavController(
        getBaseActivity(),
        getBaseActivity().getFragmentPlaceHolder()
    )

    fun <T : Any> getExtraValue(valueType: KClass<T>): T? {
        return arguments?.getString(EXTRA_DATA_VALUE)
            ?.let { objectMapper.readValue(it, valueType.java) }
    }

    fun setExtraValue(value: Any?) {
        if (arguments == null) {
            arguments = Bundle()
        }

        arguments?.putString(
            EXTRA_DATA_VALUE,
            value?.let {
                objectMapper.writeValueAsString(it)
            }
        )
    }

    fun showFragment(resId: Int, extraValue: Any? = null) {
        val bundle = extraValue?.let { value ->
            bundleOf(EXTRA_DATA_VALUE to objectMapper.writeValueAsString(value))
        }

        getNavController().navigate(resId, bundle)
    }

    fun popBackStack() {
        getNavController().popBackStack()
    }

    fun hideKeyboard(view: View? = null) {
        val imm = activity
            ?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            ?: return

        val token = (view ?: activity?.currentFocus)?.windowToken
            ?: activity?.window?.decorView?.rootView?.windowToken
            ?: View(requireContext()).windowToken

        imm.hideSoftInputFromWindow(
            token,
            0
        )
    }

    open fun openKeyboard(view: View) {
        view.requestFocus()
        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_FORCED)
    }

    open fun onBackPressed(): Boolean {
        return false
    }

}