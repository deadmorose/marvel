package ru.codegang.marvel.base

import android.content.Intent
import android.os.Bundle
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.fasterxml.jackson.databind.ObjectMapper
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.DaggerAppCompatActivity
import java.io.Serializable
import javax.inject.Inject
import kotlin.reflect.KClass

abstract class BaseActivity : DaggerAppCompatActivity() {

    companion object {
        const val EXTRA_DATA_VALUE = "DATA_VALUE"
        const val EXTRA_NAV_GRAPH = "NAV_GRAPH"

    }

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    private val objectMapper: ObjectMapper = ObjectMapper()

    abstract fun getFragmentPlaceHolder(): Int

    abstract fun getContentView(): Int

    fun getNavController() = Navigation.findNavController(this, getFragmentPlaceHolder())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(getContentView())

        getNavGraph()?.let {
            setGraph(it)
        }
    }

    fun setGraph(resId: Int) {
        intent.putExtra(EXTRA_NAV_GRAPH, resId)

        getNavController().apply {
            graph = navInflater.inflate(resId)
        }
    }

    fun <T : Any> getExtraValue(valueType: KClass<T>): T? {
        return intent?.getStringExtra(EXTRA_DATA_VALUE)?.let {
            objectMapper.readValue(it, valueType.java)
        }
    }


    fun setExtraValue(value: Any?) {
        intent?.putExtra(
            EXTRA_DATA_VALUE,
            value?.let { objectMapper.writeValueAsString(it) }
        )
    }

    fun getNavGraph(): Int? {
        return intent.getSerializableExtra(EXTRA_NAV_GRAPH) as? Int
    }

    fun <T : BaseActivity> startActivity(
        activity: KClass<T>,
        navGraph: Int? = null,
        value: Serializable? = null,
        flag: Int? = null
    ) {
        val intent = Intent(this, activity.java)

        navGraph?.let {
            intent.putExtra(EXTRA_NAV_GRAPH, navGraph)
        }

        value?.let {
            intent.putExtra(EXTRA_DATA_VALUE, it)
        }

        flag?.let {
            intent.flags = it
        }

        startActivity(intent)
    }

    fun showFragment(resId: Int, extraValue: Any? = null) {
        val bundle = Bundle()

        extraValue?.let {
            bundle.putString(
                EXTRA_DATA_VALUE,
                objectMapper.writeValueAsString(it)
            )
        }

        getNavController().navigate(
            resId,
            bundle
        )
    }
    override fun onBackPressed() {
        if (onFragmentBackPressed()) {
            return
        }

        super.onBackPressed()
    }
    private fun onFragmentBackPressed(): Boolean {
        val navHostFragment = supportFragmentManager
            .fragments
            .firstOrNull() as? NavHostFragment

        return navHostFragment?.childFragmentManager?.fragments?.any { fragment ->
            fragment is BaseFragment && fragment.isVisible && fragment.onBackPressed()
        } ?: false
    }

}