package ru.codegang.marvel.base

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

class SingleMutableLiveData<T> : MutableLiveData<T?>() {

    override fun observe(owner: LifecycleOwner, observer: Observer<in T?>) {
        super.observe(owner, Observer { t ->
            t?.let {
                observer.onChanged(it)
                postValue(null)
            }
        })
    }
}

fun SingleMutableLiveData<Unit>.call() {
    postValue(Unit)
}