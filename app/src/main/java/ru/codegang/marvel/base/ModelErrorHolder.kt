package ru.codegang.marvel.base

interface ModelErrorHolder {

    val error: SingleMutableLiveData<String>
}