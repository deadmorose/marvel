package ru.codegang.marvel.base

import androidx.lifecycle.LiveData

interface ModelBlockingHolder {

    val blocking: LiveData<Boolean>
}