package ru.codegang.marvel.settings

import okhttp3.logging.HttpLoggingInterceptor

object DefaultSettings {
    val HTTP_LOG_LEVEL = HttpLoggingInterceptor.Level.BODY
    const val NETWORK_CONNECT_TIMEOUT: Long = 20 * 1000
    const val NETWORK_READ_TIMEOUT: Long = 20 * 1000
}