package ru.codegang.marvel.network.entities

import java.io.IOException
import java.lang.Exception


object CloudExceptionCode {
    const val INVALID_RESPONSE = "SOMETHING WENT WRONG, MARVEL RETURNS INVALID RESPONSE"
}

open class CloudException(
    override var message: String? = null
): IOException("CloudException"){
    var code: String? = null
    var field: String? = null
}