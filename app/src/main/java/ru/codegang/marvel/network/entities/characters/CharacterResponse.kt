package ru.codegang.marvel.network.entities.characters

import ru.codegang.marvel.base.BaseNetworkEntity

data class CharacterResponse(
    val offset: Int,
    val limit: Int,
    val total: Int,
    val count: Int,
    val result: List<CharacterEntity>
): BaseNetworkEntity()