package ru.codegang.marvel.network.entities.characters

import ru.codegang.marvel.base.BaseNetworkEntity

data class CharacterEntity(
    val id: String,
    val name: String,
    val description: String,
    val thumbnail: String
) : BaseNetworkEntity()