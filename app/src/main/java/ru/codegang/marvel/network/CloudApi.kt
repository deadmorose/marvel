package ru.codegang.marvel.network

import retrofit2.http.GET
import retrofit2.http.Query
import ru.codegang.marvel.network.entities.characters.CharacterResponse


interface CloudApi {
    @GET("characters")
    suspend fun getCharacters(
        @Query("limit") limit: Int = 10,
        @Query("offset") offset: Int = 0,
        @Query("nameStartsWith") name: String = ""
    ):CharacterResponse


}