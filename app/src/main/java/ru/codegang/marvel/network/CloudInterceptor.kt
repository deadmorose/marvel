package ru.codegang.marvel.network

import android.util.Log
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import org.json.JSONObject
import ru.codegang.marvel.BuildConfig
import ru.codegang.marvel.network.entities.CloudException
import ru.codegang.marvel.network.entities.CloudExceptionCode
import ru.codegang.marvel.utils.md5
import java.io.BufferedReader
import java.io.InputStreamReader
import javax.inject.Inject

class CloudInterceptor @Inject constructor(

) : Interceptor {

    companion object {
        const val FIELD_CODE = "code"
        const val FIELD_STATUS = "status"
        const val FIELD_DATA = "data"
        const val FIELD_MESSAGE = "message"
    }

    @Synchronized
    override fun intercept(chain: Interceptor.Chain): Response {
        val ts = System.currentTimeMillis().toString()
        val hash = (ts + "secret" + "public").md5()
        val url: HttpUrl = chain.request().url
            .newBuilder()
            .addQueryParameter("ts", ts)
            .addQueryParameter("apikey", "public")
            .addQueryParameter("hash", hash)
            .build()


        val request = chain.request().newBuilder()
            .url(url).build()

        val response = chain.proceed(request)

        return processing(request, response)
    }


    private fun getJSONObject(response: Response): JSONObject {
        val reader = BufferedReader(InputStreamReader(response.body!!.byteStream()))
        val stringBuilder = StringBuilder()
        do {
            val line = reader.readLine()
            if (line != null) {
                stringBuilder.append(line)
            }
        } while (line != null)

        val result = stringBuilder.toString()

        return JSONObject(result)
    }

    private fun processing(
        request: Request,
        response: Response
    ): Response {

        if (request.method == "HEAD") return response

        return if (request.url.toString().startsWith(BuildConfig.HOST_URL)) {
            prepareResponse(request, response, getJSONObject(response))
        } else {
            response
        }
    }

    @Throws(CloudException::class)
    private fun prepareResponse(
        request: Request,
        response: Response,
        data: JSONObject
    ): Response {

        val error = CloudException()

        if (data.has(FIELD_CODE) && !data.isNull(FIELD_CODE)) {
            val code = response.code
            if (code != 200) {
                if (data.has(FIELD_MESSAGE) && !data.isNull(FIELD_MESSAGE)) {
                    error.message = data.getString(FIELD_MESSAGE)
                }
                throw error
            }
        } else {
            error.message = CloudExceptionCode.INVALID_RESPONSE
            throw error
        }
        val jsonText: String
        if (data.has(FIELD_DATA)) {
            jsonText = if (data.get(FIELD_DATA) is JSONObject) {
                val obj = data.getJSONObject(FIELD_DATA)
                obj.toString()
            } else {
                val objArray = data.getJSONArray(FIELD_DATA)
                objArray.toString()
            }
        } else {
            error.message = CloudExceptionCode.INVALID_RESPONSE
            throw error
        }


        val builder = Response.Builder()
            .request(response.request)
            .protocol(response.protocol)
            .code(response.code)
            .message(response.message)
            .handshake(response.handshake)
            .headers(response.headers)
            .body(jsonText.toByteArray().toResponseBody(response.body?.contentType()))
            .networkResponse(response.networkResponse)
            .cacheResponse(response.cacheResponse)
            .priorResponse(response.priorResponse)
            .sentRequestAtMillis(response.sentRequestAtMillis)
            .receivedResponseAtMillis(response.receivedResponseAtMillis)

        return builder.build()
    }

}